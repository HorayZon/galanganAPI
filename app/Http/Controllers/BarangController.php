<?php

namespace App\Http\Controllers;

use App\Barang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function index()
    {
        return response()->json([
            'Barang' => Auth::user()->barang
        ], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_barang' => 'required',
            'stok' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required',
            'kategori' => 'nullable',
            'gambar' => 'required'
        ]);


        //Handle Upload Image
        if ($request->hasFile('gambar')) {
            $fileNameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('gambar')->getClientOriginalExtension();
            $fileNameToStore = $filename.'.'.$extension;
            $path = $request->file('gambar')->storeAs('public/gambar', $fileNameToStore);
        }else {
            $fileNameToStore = 'noimage.jpg';
        }

        $nama_barang = $request->input('nama_barang');
        $stok = $request->input('stok');
        $harga = $request->input('harga');
        $deskripsi = $request->input('deskripsi');
        $kategori = $request->input('kategori');

        $barang = Auth::user()->barang()->create([
            'nama_barnag' => $nama_barang,
            'stok' => $stok,
            'harga' => $harga,
            'deskripsi' => $deskripsi,
            'kategori' => $kategori,
            'gambar' => $fileNameToStore
        ]);

        if ($barang->save()) {

        $barang->view_detail = [
            'href' => 'api/galanganapi/barang/' .$barang->id,
            'method' => 'GET'
        ];

        $message = [
            'msg' => 'Barang berhasil ditambahkan',
            'barang' => $barang
        ];

        return response()->json($message, 200);

        }

        return response()->json([
            'msg' => 'An Error Occurred'
        ], 401);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_barang' => 'required',
            'stok' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required',
            'kategori' => 'nullable',
            'gambar' => 'required'
        ]);

        if ($request->hasFile('gambar')) {
            $fileNameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('gambar')->getClientOriginalExtension();
            $fileNameToStore = $filename . '.' . $extension;
            $path = $request->file('gambar')->storeAs('public/gambar', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $nama_barang = $request->input('nama_barang');
        $stok = $request->input('stok');
        $harga = $request->input('harga');
        $deskripsi = $request->input('deskripsi');
        $kategori = $request->input('kategori');

        $barang = Barang::with('users')->findOrFail($id);

        if (!$barang->user()->where('users.id', Auth::user()->id)->first()) {
            $response = [
                'msg' => 'User bukan yang membuat postingan ini, update gagal dilakukan'
            ];

            return response()->json($response, 401);
        };

        $barang->nama_barang = $nama_barang;
        $barang->stok = $stok;
        $barang->harga = $harga;
        $barang->deskripsi = $deskripsi;
        $barang->kategori = $kategori;
        $barang->gambar = $fileNameToStore;

        if (!$barang->update()) {

            return response()->json([
                'msg' => 'Terjadi error saat melakukan update'
            ], 404);
        }

        $barang->view_barang = [
            'href' => 'api/galanganapi/barang/edit/' . $barang->id,
            'method' => 'GET'
        ];

        $response = [
            'msg' => 'Barang berhasil di update',
            'barang' => $barang
        ];

        return response()->json($response, 200);

    }

    public function show($id)
    {
        $barang = Barang::find($id);

        if (Auth::user()->id !== $barang->user_id) {

            $response = [
                'status' => 'error',
                'message' => 'Unauthorized'
            ];

            return response()->json($response, 401);
        }

        $response = [
            'status' => 'success',
            'barang' => $barang
        ];

        return response()->json($response, 200);

    }
}
