<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


use JWTAuth;
use JWTException;
use Tymon\JWTAuth\Exceptions\JWTException as TymonJWTException;
use Tymon\JWTAuth\JWTAuth as TymonJWTAuth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min : 6'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        if ($user = User::where('email' , $email)->first()) {


            $credentials = [
                'email' => $email,
                'password' => $password
            ];

            $token = null;
            try {
                if(!$token = JWTAuth::attempt($credentials))
                {
                    return response()->json([
                        'msg' => 'Email atau password Salah'
                    ], 401);
                }
            } catch (JWTException $e) {

                return response()->json([
                    'msg' => 'failed_to_create_token'
                ], 400);
            }

            $response = [
                'msg' => 'User berhasil login',
                'user' => $user,
                'token' => $token
            ];

            return response()->json($response, 201);
        }
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'notelp' => 'required'
        ]);

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $notelp = $request->input('notelp');

        $user = New User([
            'name' => $name,
            'email' => $email,
            'notelp' => $notelp,
            'password' => bcrypt($password)
        ]);

        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        if ($user->save()) {

            $token = null;

            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'msg' => 'Email or Password Incorrect'
                    ], 404);
                }
            } catch (JWTException $e) {
                return response()->json([
                    'msg' => 'failed_to_create_token'
                ], 400);
            }

            $user->login = [
                'href' => 'api/galanganapi/user/login',
                'method' => 'POST',
                'params' => 'email,password'
            ];

            $response = [
                'msg' => 'User telah dibuat',
                'user' => $user,
                'token' => $token
            ];

            return response()->json($response, 201);
        }

        $response = [
            'msg' => 'An error occurred'
        ];

        return response()->json($response, 404);

    }

    public function getAuthtenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));

    }

    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate($request->header('token'));

            return response()->json([
                'success' => true,
                'msg' => 'User berhasil logout'
            ]);
        } catch (JWTException $th) {
            return response()->json([
                'success' => false,
                'message' => 'Maaf, User tidak bisa logout'
            ], 500);
        }

    }

}
