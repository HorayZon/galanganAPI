<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $fillable = [
        'nama_barang', 'stok', 'harga', 'gambar' , 'deskripsi', 'kategori'
    ];

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
