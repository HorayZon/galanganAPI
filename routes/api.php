<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'galanganapi', 'middleware' => ['cors', 'jwt.auth']], function () {

});


Route::group(['prefix' => 'galanganapi', 'middleware' => 'cors'], function () {

    Route::post('user/register',[
        'uses' => 'AuthController@register'
    ]);

    Route::post('user/login', [
        'uses' => 'AuthController@login'
    ]);

    Route::get('user/logout', [
        'uses' => 'AuthController@logout'
    ]);

});
